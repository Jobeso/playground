#!/usr/bin/env node

const program = require('commander')
const { spawn } = require('child_process')

const run = (configuration = 'Debug', device) => {
  const cmd = device
    ? spawn('react-native', [
        'run-ios',
        '--configuration',
        configuration,
        '--device',
        device,
      ])
    : spawn('react-native', ['run-ios', '--configuration', configuration])

  cmd.stdout.on('data', data => console.log(data.toString()))
  cmd.stderr.on('data', data => console.warn(data.toString()))
  cmd.on('exit', code =>
    console.log(`child process exited with code ${code.toString()}`)
  )
}

program
  .version('0.1.0')
  .usage('./buildToDevice.js [options]')
  .arguments('[configuration] [device]')
  .action(run)
  .parse(process.argv)
