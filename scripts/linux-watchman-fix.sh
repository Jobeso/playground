#!/usr/bin/env bash
sudo sysctl fs.inotify.max_user_watches=524288
watchman watch-del $(pwd)
watchman watch-project $(pwd)
watchman shutdown-server
