#!/usr/bin/env node
const path = require('path')
const { exec } = require('child_process')

const pkg = require(path.resolve(__dirname, '../package.json'))

const filter = [
  'babel-eslint',
  'babel-jest',
  'babel-preset-react-native',
  'eslint',
  'react',
  'react-native',
]

const customDepsFilter = /^(git|http).+/

const updateDependenciesCommand = Object.keys(pkg.dependencies).reduce(
  (acc, lib) =>
    customDepsFilter.test(pkg.dependencies[lib]) || filter.includes(lib)
      ? acc
      : `${acc} ${lib}`,
  'yarn add'
)

const updateDevDependenciesCommand = Object.keys(pkg.devDependencies).reduce(
  (acc, lib) =>
    customDepsFilter.test(pkg.devDependencies[lib]) || filter.includes(lib)
      ? acc
      : `${acc} ${lib}`,
  'yarn add -D'
)

exec(updateDependenciesCommand, (err, stdout, stderr) => {
  if (err) {
    console.error(err)
  }
  if (stderr) {
    console.warn(stderr)
  }
  console.log(stdout)

  exec(updateDevDependenciesCommand, (err, stdout, stderr) => {
    if (err) {
      console.error(err)
    }
    if (stderr) {
      console.warn(stderr)
    }
    console.log(stdout)
  })
})
