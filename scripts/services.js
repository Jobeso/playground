const path = require('path')
const { forEachObjIndexed, isEmpty, repeat } = require('ramda')

const exit = errorCode => process.exit(errorCode)

const error = (err, ...params) =>
  console.error(err, ...params) && process.exit()

const warn = (err, ...params) => console.warn(err, ...params)

const print = (text, ...params) =>
  text ? console.log(text, ...params) : console.log()

const printHeadline = text => {
  console.log(`########## ${text} ##########`)
  console.log()
}

const printOption = (option, description = '') =>
  console.log(
    `${repeat(' ', 10).join('')}${
      option.length >= 28
        ? option
        : option + repeat(' ', 28 - option.length).join('')
    } ${description}`
  )

const resolve = p => path.resolve(__dirname, p)

const isValueDefined = (path, obj) =>
  path.split('.').reduce((acc, key) => (acc ? acc[key] : undefined), obj)

const filterEmptyValues = (object, array = []) => {
  const getFilledObjectPaths = (iterationObject, path = []) =>
    forEachObjIndexed(
      (value, key) =>
        typeof value === 'string' && !isEmpty(value)
          ? array.push([...path, key].join('.'))
          : getFilledObjectPaths(value, [...path, key]),
      iterationObject
    )
  getFilledObjectPaths(object)
  return array
}

module.exports = {
  exit,
  error,
  filterEmptyValues,
  isValueDefined,
  print,
  printHeadline,
  printOption,
  resolve,
  warn,
}
